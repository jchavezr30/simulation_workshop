# Simulation Framwork for the workshop in Modeling and Simulation

Here we include all the dependencies to compile the simulation tools that we use
to model different microarcitectures.

First, if you have a linux distro (ubuntu is the best one for our tools),
you can directly install all the dependencies in your system. However, if
you prefer to have the tools created an portable, please dowload our docker
image that includes all the functionalities and dependencies.

- The dependencies for gem5 can be found here:
https://www.gem5.org/documentation/general_docs/building

- Dependencies for QEMU are listed in the next link:
https://wiki.qemu.org/Hosts/Linux

- For the RISC-V tools dependencies are listed here:
https://github.com/riscv-collab/riscv-gnu-toolchain

### Install Docker image

To install the pre-build docker image, please download the next file using the
next command:


```
git clone git@gitlab.com:julax921/simulation_workshop.git
cd simulation_workshop
```

Next, load the docker image using the next command:

```
docker load -i tools.tar
```

When the docker image is loaded in our system, we can run it using the Makefile:

```
make run
```

Once the image is running you can use it to compile all the tools used during this
workshop. To stop the docker container please run exit in the terminal.

### Compiling the riscv tools

First git clone the tools using the next command:

```
git clone https://github.com/riscv-collab/riscv-gnu-toolchain.git
```

To compile the linux version of the tools run:

```
cd riscv-gnu-toolchain
./configure --prefix=<select the path>
make -j <select number of threads> linux
make install
```

### Compiling gem5

Follow the next commands to clone and compile the gem5 simulator

```
git clone https://gem5.googlesource.com/public/gem5
cd gem5
scons build/RISCV/gem5.opt -j <select the number of threads>
```

